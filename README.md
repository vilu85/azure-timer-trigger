## Azure timer trigger for [assignment 10.11](https://gitlab.com/vilu85/buutti-education/-/tree/main/Lecture11/Assignment11.10) : [friendsBuuk](https://gitlab.com/vilu85/friendsbuuk)

### What it does

This function app initializes the Azure timer function, which fetches a new user from the [NameFake.com API](https://en.namefake.com/api) every 10 seconds and sends the user to the friendsBuuk API.

When running, the function app checks whether it is already running, which prevents that more than one 10-second fetch instance cannot be running.

### How to run

To run a function app, either launch it locally or deploy it to Azure as a function app. The host url of the friendsBuuk server can be changed using the environment variable `APIURL`. Please note that `APIURL` should be the base url without endpoint route, for example when running locally **http://localhost:3000** or in Azure **https://<your function app>.azurewebsites.net**

An environment variable can either be given at runtime or defined in an .env file.