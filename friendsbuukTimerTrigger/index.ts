import { type AzureFunction, type Context } from '@azure/functions';
import axios from 'axios';
import 'dotenv/config';

const friendsbuukApiUrl = `${process.env.APIURL ?? 'https://friendsbuuk.azurewebsites.net'}/friends`;

interface FakeNameResponse extends Response {
    name: string;
    username: string;
    email_u: string;
    email_d: string;
}

interface FriendApiRequest {
    name: string;
    username: string;
    email: string;
}

async function getNewFriend(): Promise<FakeNameResponse> {
    try {
        // Fetch a new user from NameFakeApi
        const response = await axios.get<FakeNameResponse>('https://api.namefake.com');
        const fakeNameResponse: FakeNameResponse = response.data;

        console.log(`NameFakeApi responsed with a new name: ${fakeNameResponse.name}`);
        return fakeNameResponse;
    } catch (error) {
        console.log('Error: (getNewFriend)', error.message);
    }
}

async function addNewFriend(newFriend: FriendApiRequest): Promise<void> {
    try {
        // Create a new friend
        console.log(`Posting new friend to ${friendsbuukApiUrl}:`, newFriend);

        // Post the new friend to the friendsbuuk Api
        const friendsbuukResponse = await axios.post(friendsbuukApiUrl, { ...newFriend });
        console.log('Friendsbuuk response:', friendsbuukResponse.data);
    } catch (error) {
        console.log('Error (addNewFriend):', error.message);
    }
}

const timerTrigger: AzureFunction = function (context: Context, myTimer: any): void {
    try {
        // Check if the function is already running to prevent overlapping executions
        if (context.bindings.timerTrigger) {
            context.log('Function is already running. Skipping execution.');
            return;
        }

        const timeStamp = new Date().toISOString();

        if (myTimer.isPastDue) {
            context.log('Timer function is running late!');
        }
        context.log('Timer trigger function ran!', timeStamp);

        /**
         * Azure cron has a limitation which restricts timer functions be triggered more than once per minute.
         * By using JavaScript's setInterval we can trigger addFriend function every 10 seconds though.
         */

        /**
         * Callback function for setInterval
         */
        async function intervalCallback(): Promise<void> {
            // Fetch a new user from namefake api
            await getNewFriend()
                .then((fakenameResponse) => {
                    // Create a new friend for the friendsbuuk
                    const newFriend: FriendApiRequest = {
                        name: fakenameResponse.name,
                        username: fakenameResponse.username,
                        email: `${fakenameResponse.email_u}@${fakenameResponse.email_d}`
                    };

                    return newFriend;
                })
                .then((newFriend) => {
                    // Post new friend to friendsbuuk
                    addNewFriend(newFriend)
                        .then(() => {})
                        .catch((error) => {
                            context.log('API call failed:', error);
                        })
                }).catch((error) => {
                    console.log(error)
                });
        }

        // Schedule the repeated API calls every 10 seconds if setInterval timer is not bind yet
        context.bindings.interval = setInterval(intervalCallback, 10000);
    } catch (error) {
        console.log('Error: (timerTrigger)', error.message);
    }
};

export default timerTrigger;
